package util;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

public class SerialiserToJson {

    public static <T> void serialise(Set<T> set, String jsonPath) {
        try (FileWriter fileWriter = new FileWriter(jsonPath)) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.writeValue(fileWriter, set);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
