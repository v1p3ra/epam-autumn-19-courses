package dao;

import entity.Album;
import entity.Performer;
import entity.Song;
import util.ConnectionToDB;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class PerformerJdbcDAO {

    public Set<Performer> getAll() throws DAOException {
        String query = "SELECT * FROM full_info;";
        Set<Performer> performers = new TreeSet<>();
        Set<Album> allAlbums = new TreeSet<>();
        List<Song> allSongs = new ArrayList<>();

        try (Connection connection = ConnectionToDB.createConnection();
             Statement statement = connection.createStatement()) {

            ResultSet resultSet = statement.executeQuery(query);

            // Получение и обработка "сырых" данных
            while (resultSet.next()) {
                Performer performer = new Performer();
                Album album = new Album();
                Song song = new Song();

                performer.setId(resultSet.getLong("performer_id"));
                performer.setName(resultSet.getString("name"));

                album.setId(resultSet.getLong("album_id"));
                album.setTitle(resultSet.getString("title"));
                album.setYear(resultSet.getInt("year"));
                album.setPerformer(performer);

                song.setId(resultSet.getLong("song_id"));
                song.setSong(resultSet.getString("song"));
                song.setDuration(resultSet.getString("duration"));
                song.setAlbum(album);

                allSongs.add(song);
                allAlbums.add(album);
                performers.add(performer);
            }

            // Группировка всех песен по альбомам, а альбомов - по исполнителям
            for (Performer performer : performers) {
                List<Album> performerAlbums = new ArrayList<>();
                performer.setAlbums(performerAlbums);

                for (Album album : allAlbums) {
                    List<Song> albumSongs = new ArrayList<>();

                    if (album.getPerformer().equals(performer)) {
                        performerAlbums.add(album);

                        for (Song song : allSongs) {
                            if (song.getAlbum().equals(album)) {
                                albumSongs.add(song);
                            }
                        }

                        album.setSongs(albumSongs);
                    }
                }
            }
        } catch (SQLException e) {
            System.out.println("Can't retrieve list of performers");
            throw new DAOException("Can't retrieve list of performers", e);
        }

        return performers;
    }
}
