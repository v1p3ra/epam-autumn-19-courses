import dao.DAOException;
import dao.PerformerJdbcDAO;
import entity.Album;
import entity.Performer;
import entity.Song;
import util.SerialiserToJson;

import java.util.List;
import java.util.Set;

public class Main {
    public static void main(String[] args) throws DAOException {
        PerformerJdbcDAO performerJdbcDAO = new PerformerJdbcDAO();
        Set<Performer> performers = performerJdbcDAO.getAll();
        String jsonPath = "C:/Projects/MongoDB/Performers.json";
        SerialiserToJson.serialise(performers, jsonPath);

        for (Performer performer : performers) {
            System.out.println("Исполнитель: " + performer.getId() + ", " + performer.getName());
            List<Album> albums = performer.getAlbums();

            for (Album album : albums) {
                System.out.println("Альбом: " + album.getId() + ", " + album.getTitle() + ", " + album.getYear());
                List<Song> songs = album.getSongs();

                for (Song song : songs) {
                    System.out.println("Песня: " + song.getSong() + ", " + song.getDuration());
                }

                System.out.println();
            }

            System.out.println();
        }
    }
}
