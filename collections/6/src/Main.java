import data.Cache;

public class Main {

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            int dataId = (int) Math.round(Math.random() * 300);
            System.out.println("Запрошен объект с id: " + dataId);

            long startTime = System.currentTimeMillis();
            Object requestedData = Cache.getDataById(dataId);
            long endTime = System.currentTimeMillis();

            System.out.println("Объект " + requestedData + " получен за " + (endTime - startTime) + " мс\n");
        }
    }
}