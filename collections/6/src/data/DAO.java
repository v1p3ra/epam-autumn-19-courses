package data;

import java.util.HashMap;
import java.util.Map;

class DAO {
    private static Map<Integer, Object> allData = new HashMap<>();

    static {
        for (int i = 1; i <= 200; i++) {    // Итерация с 1, чтобы ID начинался не с 0
            allData.put(i, i);
        }
    }

    static Map<Integer, Object> getDataForCache() {
        Map<Integer, Object> cache = new HashMap<>();

        for (int i = 1; i <= 50; i++) {
            cache.put(i, allData.get(i));
        }

        return cache;
    }

    static Object getDataById(Integer requestedObjectId) {
        // Имитация продолжительного по времени доступа к данным :)
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println("Обработка данных была прервана");
        }

        // Добавление файла, отсутствующего в "медленном" источнике данных
        if (!allData.containsKey(requestedObjectId)) {
            try {
                Thread.sleep(800);
            } catch (InterruptedException e) {
                System.out.println("Обработка данных была прервана");
            }

            allData.put(requestedObjectId, requestedObjectId);
            System.out.println("Файл загружен с удалённого сервера");
        }

        return allData.get(requestedObjectId);
    }
}