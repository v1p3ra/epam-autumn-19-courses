package data;

import java.util.Map;

public class Cache {
    private static Map<Integer, Object> cache;

    static {
        cache = DAO.getDataForCache();
    }

    public static Object getDataById(Integer requestedObjectId) {
        System.out.println("Поиск данных в кэше...");
        if (!cache.containsKey(requestedObjectId)) {
            System.out.println("Объект не найден в кэше");
            System.out.println("Запрос объекта из основного источника данных");
            cache.put(requestedObjectId, DAO.getDataById(requestedObjectId));
        } else {
            System.out.println("Объект найден в кэше");
        }

        return cache.get(requestedObjectId);
    }
}