import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class RequestHandler {
    static Queue<Request> requestQueue = new ConcurrentLinkedQueue<>();

    public static void main(String[] args) throws InterruptedException {
        Thread executor1 = new Thread(new RequestExecutor(), "#1");
        Thread executor2 = new Thread(new RequestExecutor(), "#2");
        Thread executor3 = new Thread(new RequestExecutor(), "#3");

        for (int i = 0; i < 10; i++) {
            addRequest();
        }

        executor1.start();
        executor2.start();
        executor3.start();
        Thread.sleep(10_000);

        for (int i = 0; i < 5; i++) {
            addRequest();
            Thread.sleep(3_000);
        }

        executor1.interrupt();
        executor2.interrupt();
        executor3.interrupt();
    }

    public static void addRequest() {
        long time = Math.round(Math.random() * 10);
        requestQueue.add(new Request(time));
        System.out.printf("Поступил новый запрос: требуемое время выполнения - %d сек.\n", time);
    }
}