public class Request {
    private long time;

    public Request(long time) {
        this.time = time;
    }

    public long getTime() {
        return time;
    }
}