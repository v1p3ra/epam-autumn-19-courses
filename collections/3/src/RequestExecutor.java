public class RequestExecutor implements Runnable {

    @Override
    public void run() {
        String threadName = Thread.currentThread().getName();

        while (RequestHandler.requestQueue.peek() != null || !Thread.currentThread().isInterrupted()) {
            Request request = RequestHandler.requestQueue.poll();

            if (request != null) {
                try {
                    long requestComplexity = request.getTime();
                    System.out.printf("Исполнитель %s приступил к обработке запроса. Сложность запроса - %d сек.\n",
                            threadName, requestComplexity);
                    Thread.sleep(request.getTime() * 1000);
                    System.out.printf("Исполнитель %s успешно обработал запрос\n", threadName);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        System.out.printf("Очередь запросов пуста. Исполнитель %s завершает свою работу\n", threadName);
    }
}