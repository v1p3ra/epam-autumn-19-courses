import java.util.ArrayDeque;
import java.util.Deque;

public class ArrayDequeBenchmark {

    public static void main(String[] args) {
        Deque<Object> queue = new ArrayDeque<>();
        int insertFirstCount = 10_000_000;
        int insertLastCount = 10_000_000;
        int deleteFirstCount = 1_000_000;
        int deleteLastCount = 1_000_000;

        // Бенчмарк 1
        System.out.println("Вставка " + insertFirstCount + " элементов в начало очереди...");
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < insertFirstCount; i++) {
            queue.addFirst(new Object());
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Затрачено времени для ArrayDeque: " + (endTime - startTime) + " мс");


        // Бенчмарк 2
        System.out.println("\nВставка " + insertLastCount + " элементов в конец очереди...");
        startTime = System.currentTimeMillis();
        for (int i = 0; i < insertLastCount; i++) {
            queue.addLast(new Object());
        }
        endTime = System.currentTimeMillis();
        System.out.println("Затрачено времени для ArrayDeque: " + (endTime - startTime) + " мс");


        // Бенчмарк 3
        System.out.println("\nУдаление " + deleteFirstCount + " элементов из начала очереди...");
        startTime = System.currentTimeMillis();
        for (int i = 0; i < deleteFirstCount; i++) {
            queue.removeFirst();
        }
        endTime = System.currentTimeMillis();
        System.out.println("Затрачено времени для ArrayDeque: " + (endTime - startTime) + " мс");


        // Бенчмарк 4
        System.out.println("\nУдаление " + deleteLastCount + " элементов с конца очереди...");
        startTime = System.currentTimeMillis();
        for (int i = 0; i < deleteLastCount; i++) {
            queue.removeLast();
        }
        endTime = System.currentTimeMillis();
        System.out.println("Затрачено времени для ArrayDeque: " + (endTime - startTime) + " мс");
    }
}
