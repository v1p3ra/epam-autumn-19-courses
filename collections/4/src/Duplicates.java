import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class Duplicates {

    public static void main(String[] args) {
        Collection<Integer> collection = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 2; j++) {
                collection.add(i);
            }
        }

        System.out.println("Исходное содержимое коллекции:");
        System.out.println(collection);


        collection = new HashSet<>(collection);

        System.out.println("Содержимое коллекции без дубликатов:");
        System.out.println(collection);
    }
}