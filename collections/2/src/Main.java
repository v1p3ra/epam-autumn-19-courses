import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        int capacity = 100;
        List<Order> orders = new ArrayList<>(capacity);
        List<Order> filteredOrders = new ArrayList<>();

        for (int i = 0; i < capacity; i++) {
            double price = Math.round(Math.random() * 100);
            long idStatus = Math.round(Math.random() * 10);
            OrderStatus status;

            if (idStatus == 1) {
                status = OrderStatus.NOT_STARTED;
            } else if (idStatus == 2) {
                status = OrderStatus.PROCESSING;
            } else {
                status = OrderStatus.COMPLETED;
            }

            orders.add(new Order(i + 1, BigDecimal.valueOf(price), status));
        }

        // Способ фильтрации 1
        for (Order order : orders) {
            if (order.getPrice().compareTo(BigDecimal.valueOf(40L)) >= 0 &&
                    order.getPrice().compareTo(BigDecimal.valueOf(70L)) <= 0) {
                filteredOrders.add(order);
            }
        }

        System.out.println("Количество заказов, удовлетворяющих фильтру: " + filteredOrders.size());
        System.out.println("\nСпособ №1");

        for (Order order : filteredOrders) {
            System.out.println(order);
        }

        // Способ фильтрации 2
        System.out.println("\nСпособ №2");
        List<Order> anotherFilteredOrders = orders.stream()
                .filter(order -> order.getPrice().compareTo(BigDecimal.valueOf(40L)) >= 0 &&
                        order.getPrice().compareTo(BigDecimal.valueOf(70L)) <= 0)
                .collect(Collectors.toList());
        anotherFilteredOrders.forEach(System.out::println);
    }


    public enum OrderStatus {
        NOT_STARTED, PROCESSING, COMPLETED
    }

    public static class Order {
        private long id;
        private BigDecimal price;
        private OrderStatus status;

        Order(long id, BigDecimal price, OrderStatus status) {
            this.id = id;
            this.price = price;
            this.status = status;
        }

        public long getId() {
            return id;
        }

        public OrderStatus getStatus() {
            return status;
        }

        public void setStatus(OrderStatus status) {
            this.status = status;
        }

        public BigDecimal getPrice() {
            return price;
        }

        public void setPrice(BigDecimal price) {
            this.price = price;
        }

        @Override
        public String toString() {
            return "Заказ: " +
                    "id = " + id +
                    ", price = " + price +
                    ", status = " + status;
        }
    }
}