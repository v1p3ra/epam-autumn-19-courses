# Java Collections

> 2. Add numeric field to `Order` class, use it to filter collection of orders by some criteria (more than 50 order items for example).

В задаче были использованы два способа фильтрации: с использованием цикла for-each и с применением Stream API.
Фильтрация производилась по цене заказа (поле price типа BigDecimal). Для наглядности в сущность Order было добавлено поле id типа long.