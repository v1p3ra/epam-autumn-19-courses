import payment.PaymentMethod;
import payment.PaymentTerminal;

public class Main {

    public static void main(String[] args) {
        PaymentTerminal paymentTerminal = new PaymentTerminal();

        // Клиент 1
        paymentTerminal.setPaymentMethod(PaymentMethod.CARD);
        paymentTerminal.setBonusProgram(true);
        System.out.println("Клиент 1");
        paymentTerminal.makePayment(1500, 2000, 1234567);
        paymentTerminal.makePayment(1000, 2500, 1234567);

        try {
            paymentTerminal.makePayment(1500, 1200, 1234567);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        // Клиент 2
        paymentTerminal.setPaymentMethod(PaymentMethod.CASH);
        paymentTerminal.setBonusProgram(true);
        System.out.println("\nКлиент 2");
        paymentTerminal.makePayment(800, 1000, 2233445);
        paymentTerminal.makePayment(700, 660, 2233445);

        // Клиент 3
        paymentTerminal.setPaymentMethod(PaymentMethod.CASH);
        paymentTerminal.setBonusProgram(false);
        System.out.println("\nКлиент 3");
        paymentTerminal.makePayment(800, 1000, 9876543);
    }
}
