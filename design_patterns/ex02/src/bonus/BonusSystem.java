package bonus;

import java.util.HashMap;
import java.util.Map;

public class BonusSystem {
    private static Map<Integer, Integer> bonusesDB = new HashMap<>();

    public static void addInformation(int clientPhoneNumber, int bonusesCount) {
        bonusesDB.put(clientPhoneNumber, bonusesCount);
    }

    public static int getInformation(int clientPhoneNumber) {
        return bonusesDB.getOrDefault(clientPhoneNumber, 0);
    }
}
