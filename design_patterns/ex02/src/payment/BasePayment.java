package payment;

import bonus.BonusSystem;

public class BasePayment implements Payment {

    @Override
    public int withdraw(int orderCost, int balance, int clientPhoneNumber) {
        int result;

        if (orderCost > balance + BonusSystem.getInformation(clientPhoneNumber)) {
            throw new IllegalArgumentException("Недостаточно средств для оплаты выбранным способом");
        } else {
            result = orderCost;
        }

        return result;
    }
}
