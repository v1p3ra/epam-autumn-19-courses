package payment;

public class PaymentTerminal {
    private Payment payment;
    private PaymentMethod paymentMethod;
    private boolean bonusProgram;

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public void setBonusProgram(boolean bonusProgram) {
        this.bonusProgram = bonusProgram;
    }

    public void makePayment(int orderCost, int balance, int clientPhoneNumber) {
        System.out.println("Сумма заказа: " + orderCost + " руб.");
        System.out.print("Использование бонусной системы: ");
        if (bonusProgram) {
            System.out.println("да");
        } else {
            System.out.println("нет");
        }

        payment = new BasePayment();
        init();
        payment.withdraw(orderCost, balance, clientPhoneNumber);
    }

    private void init() {
        if (bonusProgram) {
            payment = new PaymentWithBonuses(payment);
        }

        switch (paymentMethod) {
            case CARD:
                payment = new PaymentByCard(payment);
                break;
            case CASH:
                payment = new PaymentByCash(payment);
                break;
            default:
                break;
        }
    }
}
