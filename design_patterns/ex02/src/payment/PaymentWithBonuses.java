package payment;

import bonus.BonusSystem;

public class PaymentWithBonuses implements Payment {
    private Payment payment;

    public PaymentWithBonuses(Payment payment) {
        this.payment = payment;
    }

    @Override
    public int withdraw(int orderCost, int balance, int clientPhoneNumber) {
        int result = payment.withdraw(orderCost, balance, clientPhoneNumber);
        int bonuses = BonusSystem.getInformation(clientPhoneNumber);

        if (result >= bonuses) {
            result -= bonuses;
            System.out.println("Списано " + bonuses + " бонусов");
            bonuses = 0;
        } else {
            bonuses -= result;
            System.out.println("Списано " + result + " бонусов");
            result = 0;
        }

        int addBonuses = (int) (result * 0.05);
        System.out.println("Начислено " + addBonuses + " бонусов");
        BonusSystem.addInformation(clientPhoneNumber, bonuses + addBonuses);

        return result;
    }
}
