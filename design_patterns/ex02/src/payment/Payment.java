package payment;

public interface Payment {

    int withdraw(int orderCost, int balance, int clientPhoneNumber);
}
