package payment;

public class PaymentByCard implements Payment {
    private Payment payment;

    public PaymentByCard(Payment payment) {
        this.payment = payment;
    }

    @Override
    public int withdraw(int orderCost, int balance, int clientPhoneNumber) {
        System.out.println("Способ оплаты: карта");
        int result = payment.withdraw(orderCost, balance, clientPhoneNumber);
        System.out.println("Оплачено: " + result + " руб.\n");

        return result;
    }
}
