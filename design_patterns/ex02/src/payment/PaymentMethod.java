package payment;

public enum PaymentMethod {
    CARD("карта"),
    CASH("наличные");

    private String title;

    PaymentMethod(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
