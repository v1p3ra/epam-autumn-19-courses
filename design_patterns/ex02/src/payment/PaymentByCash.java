package payment;

public class PaymentByCash implements Payment {
    private Payment payment;

    public PaymentByCash(Payment payment) {
        this.payment = payment;
    }

    @Override
    public int withdraw(int orderCost, int balance, int clientPhoneNumber) {
        System.out.println("Способ оплаты: наличные");
        int result = payment.withdraw(orderCost, balance, clientPhoneNumber);
        System.out.println("Депозит: " + balance + " руб.");
        System.out.println("Оплачено: " + result + " руб.");
        int change = balance - result;
        System.out.println("Ваша сдача: " + change + " руб.\n");

        return result;
    }
}
