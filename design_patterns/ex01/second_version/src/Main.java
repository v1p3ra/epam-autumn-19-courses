import restaurant.Order;
import restaurant.Waiter;
import restaurant.bar.Bar;
import restaurant.kitchen.Kitchen;
import restaurant.kitchen.PizzaCookingType;

public class Main {
    public static void main(String[] args) {
        Kitchen kitchen = new Kitchen(PizzaCookingType.PIZZA_SHOP);
        Bar bar = new Bar();

        Waiter waiter = new Waiter(kitchen, bar);
        Client client = new Client(waiter);
        Order order = client.makeOrder();

        if (order.getPizza() == null && order.getDrink() == null) {
            System.out.println("\nКлиент уходит, так ничего и не заказав");
        }
    }
}
