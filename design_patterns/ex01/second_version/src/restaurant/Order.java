package restaurant;

import restaurant.menu.Drink;
import restaurant.menu.Pizza;

public class Order {
    private Pizza pizza;
    private Drink drink;

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }
}
