package restaurant.bar;

import restaurant.menu.Drink;
import restaurant.menu.DrinkType;

public class Bar {

    public Drink getDrink(DrinkType drinkType) {
        System.out.printf("\nБармен: \"Начинаю делать %s напиток\"\n", drinkType.getName());
        try {
            Thread.sleep(3_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Бармен: \"Напиток %s готов\"\n", drinkType.getName());

        return new Drink(drinkType);
    }
}
