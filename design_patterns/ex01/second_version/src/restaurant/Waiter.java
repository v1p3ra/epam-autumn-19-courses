package restaurant;

import restaurant.bar.Bar;
import restaurant.kitchen.Kitchen;
import restaurant.menu.Drink;
import restaurant.menu.DrinkType;
import restaurant.menu.Pizza;
import restaurant.menu.PizzaType;

public class Waiter {
    private Kitchen kitchen;
    private Bar bar;

    public Waiter(Kitchen kitchen, Bar bar) {
        this.kitchen = kitchen;
        this.bar = bar;
    }

    public Order getOrder(PizzaType pizzaType, DrinkType drinkType) {
        Pizza pizza = null;
        Drink drink = null;
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (pizzaType == null && drinkType != null) {
            System.out.printf("\nОфициант: \"Вы заказали: %s напиток\"\n", drinkType.getName());
            try {
                Thread.sleep(1_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Официант несёт заказ в бар");
            drink = bar.getDrink(drinkType);
        } else if (pizzaType != null && drinkType == null) {
            System.out.printf("\nОфициант: \"Вы заказали: пицца %s\"\n", pizzaType.getName());
            try {
                Thread.sleep(1_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Официант несёт заказ на кухню");
            pizza = kitchen.getPizza(pizzaType);
        } else if (pizzaType != null) {
            System.out.printf("\nОфициант: \"Вы заказали: пицца %s и %s напиток\"\n", pizzaType.getName(),
                    drinkType.getName());
            try {
                Thread.sleep(1_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Официант несёт заказ на кухню и в бар");
            pizza = kitchen.getPizza(pizzaType);
            drink = bar.getDrink(drinkType);
        }

        Order completedOrder = new Order();
        completedOrder.setPizza(pizza);
        completedOrder.setDrink(drink);
        if (pizza != null || drink != null) {
            System.out.println("\nОфициант относит заказ клиенту");
        }

        return completedOrder;
    }
}
