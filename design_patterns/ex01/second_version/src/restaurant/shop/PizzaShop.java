package restaurant.shop;

import restaurant.menu.Pizza;
import restaurant.menu.PizzaType;

public class PizzaShop {

    public Pizza sale(PizzaType pizzaType) {
        System.out.println("\nЗаказ принят в магазине пиццы");
        try {
            Thread.sleep(1_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Приготовление...");
        try {
            Thread.sleep(10_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("\"Пицца %s готова\"\n", pizzaType.getName());

        return new Pizza(pizzaType);
    }
}
