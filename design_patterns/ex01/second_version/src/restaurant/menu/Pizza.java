package restaurant.menu;

public class Pizza {
    private PizzaType type;

    public Pizza(PizzaType type) {
        this.type = type;
    }

    public PizzaType getType() {
        return type;
    }
}
