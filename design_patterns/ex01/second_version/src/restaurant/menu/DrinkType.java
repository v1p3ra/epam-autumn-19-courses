package restaurant.menu;

public enum DrinkType {
    NON_ALCOHOLIC("безалкогольный"),
    ALCOHOLIC("алкогольный");

    private String name;

    DrinkType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
