package restaurant.kitchen;

import restaurant.menu.Pizza;
import restaurant.menu.PizzaType;

public class PizzaMaker {

    public Pizza giveOrder(PizzaType pizzaType) {
        System.out.printf("\nПицца-мейкер: \"Начинаю готовить пиццу %s\"\n", pizzaType.getName());
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Пицца-мейкер: \"Пицца %s готова\"\n", pizzaType.getName());

        return new Pizza(pizzaType);
    }
}
