package restaurant.kitchen;

import restaurant.menu.Pizza;
import restaurant.menu.PizzaType;
import restaurant.shop.PizzaShop;

public class Kitchen {
    private PizzaCookingType pizzaCookingType;

    public Kitchen(PizzaCookingType pizzaCookingType) {
        this.pizzaCookingType = pizzaCookingType;
    }

    public Pizza getPizza(PizzaType pizzaType) {
        Pizza pizza;

        if (pizzaCookingType == PizzaCookingType.PIZZA_MAKER) {
            pizza = new PizzaMaker().giveOrder(pizzaType);
        } else {
            pizza = new PizzaShop().sale(pizzaType);
        }

        return pizza;
    }
}
