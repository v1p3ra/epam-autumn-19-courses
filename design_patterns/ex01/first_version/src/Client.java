import restaurant.Order;
import restaurant.Waiter;
import restaurant.menu.DrinkType;
import restaurant.menu.PizzaType;

public class Client {
    private Waiter waiter;

    public Client(Waiter waiter) {
        this.waiter = waiter;
    }

    public Order makeOrder() {
        System.out.println("Клиент обдумывает заказ...");
        try {
            Thread.sleep(1_500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        PizzaType pizzaType = null;
        DrinkType drinkType = null;

        // Генерация типа пиццы
        int pizzaTypeId = (int) (Math.random() * (PizzaType.values().length + 1));
        System.out.println("Сгенерирован ID пиццы: " + pizzaTypeId);

        if (pizzaTypeId > PizzaType.values().length - 1) {
            System.out.println("Клиент: \"Пиццу я не хочу, закажу только напиток\"");
        } else {
            pizzaType = PizzaType.values()[pizzaTypeId];
            System.out.printf("Клиент: \"Официант, будьте добры пиццу %s\"\n", pizzaType.getName());
            System.out.println("Клиент: \"И напиток...\"");
        }

        // Генерация типа напитка
        try {
            Thread.sleep(1_500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int drinkTypeId = (int) (Math.random() * (DrinkType.values().length + 1));
        System.out.println("Сгенерирован ID напитка: " + drinkTypeId);

        if (drinkTypeId > DrinkType.values().length - 1) {
            System.out.println("Клиент: \"...пожалуй, напиток не буду заказывать\"");
        } else {
            drinkType = DrinkType.values()[drinkTypeId];
            System.out.printf("Клиент: \"...%s напиток, пожалуйста\"\n", drinkType.getName());
        }

        return waiter.getOrder(pizzaType, drinkType);
    }
}
