import restaurant.*;
import restaurant.bar.Bar;

public class Main {
    public static void main(String[] args) {
        PizzaService pizzaService = PizzaServiceFactory.getPizzaService(PizzaServiceFactory.PIZZA_MAKER);
        Bar bar = new Bar();

        if (pizzaService == null) {
            System.out.println("Выбран некорректный тип работы кухни ресторана");
            return;
        }

        Waiter waiter = new Waiter(pizzaService, bar);
        Client client = new Client(waiter);
        Order order = client.makeOrder();

        if (order.getPizza() == null && order.getDrink() == null) {
            System.out.println("\nКлиент уходит, так ничего и не заказав");
        }
    }
}
