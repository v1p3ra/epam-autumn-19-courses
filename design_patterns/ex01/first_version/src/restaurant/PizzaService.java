package restaurant;

import restaurant.menu.Pizza;
import restaurant.menu.PizzaType;

public interface PizzaService {

    Pizza getPizza(PizzaType pizzaType);
}
