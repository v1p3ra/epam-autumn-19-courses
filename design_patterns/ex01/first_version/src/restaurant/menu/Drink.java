package restaurant.menu;

public class Drink {
    private DrinkType type;

    public Drink(DrinkType type) {
        this.type = type;
    }

    public DrinkType getType() {
        return type;
    }
}
