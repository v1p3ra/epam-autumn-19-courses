package restaurant.menu;

public enum PizzaType {
    FOUR_CHEESE("Четыре сыра"),
    MARGHERITA("Маргарита"),
    MARINARA("Маринара"),
    MEAT("Мясная"),
    NEAPOLITAN("Неаполитанская");

    private String name;

    PizzaType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
