package restaurant;

import restaurant.kitchen.PizzaMaker;
import restaurant.shop.PizzaShop;

public final class PizzaServiceFactory {
    public static final int PIZZA_MAKER = 1;
    public static final int PIZZA_SHOP = 2;

    public static PizzaService getPizzaService(int cookingType) {
        switch (cookingType) {
            case PIZZA_MAKER:
                return new PizzaMaker();
            case PIZZA_SHOP:
                return new PizzaShop();
            default:
                return null;
        }
    }
}
