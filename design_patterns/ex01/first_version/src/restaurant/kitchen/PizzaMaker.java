package restaurant.kitchen;

import restaurant.PizzaService;
import restaurant.menu.Pizza;
import restaurant.menu.PizzaType;

public class PizzaMaker implements PizzaService {

    @Override
    public Pizza getPizza(PizzaType pizzaType) {
        return giveOrder(pizzaType);
    }

    public Pizza giveOrder(PizzaType pizzaType) {
        System.out.printf("\nПицца-мейкер: \"Начинаю готовить пиццу %s\"\n", pizzaType.getName());
        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("Пицца-мейкер: \"Пицца %s готова\"\n", pizzaType.getName());

        return new Pizza(PizzaType.valueOf(pizzaType.toString()));
    }
}