package com.epam.calculator;

import org.junit.Test;

public class CalculatorTestNegative {
    @Test(expected = IllegalArgumentException.class)
    public void testDivideByZero() {
        Calculator.divide(2, 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSqrtFromNegativeNumber() {
        Calculator.sqrt(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNotNaturalIsPrime() {
        Calculator.isPrime(0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFibonacciNegativeNumber() {
        Calculator.getFibonacciNumber(-10);
    }
}
