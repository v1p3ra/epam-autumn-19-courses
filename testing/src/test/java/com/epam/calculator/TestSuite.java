package com.epam.calculator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        CalculatorTestPositive.class,
        CalculatorTestNegative.class
})

public class TestSuite {
}
