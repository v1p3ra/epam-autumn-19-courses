package com.epam.calculator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;

@PowerMockIgnore({"com.sun.org.apache.xerces.*", "javax.xml.*", "org.xml.*", "org.w3c.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({NumberRowSupplier.class})

public class CalculatorTestPositive {
    @Test
    public void testSum() {
        assertEquals(11, Calculator.sum(5, 6));
    }

    @Test
    public void testSumWithNegative() {
        assertEquals(-15, Calculator.sum(-10, -5));
    }

    @Test
    public void testSumWithPositiveAndNegative() {
        assertEquals(0, Calculator.sum(-5, 5));
    }

    @Test
    public void testSubtract() {
        assertEquals(50, Calculator.subtract(100, 50));
    }

    @Test
    public void testSubtractWithNegative() {
        assertEquals(5, Calculator.subtract(-10, -15));
    }

    @Test
    public void testSubtractWithPositiveAndNegative() {
        assertEquals(60, Calculator.subtract(50, -10));
    }

    @Test
    public void testMultiply() {
        assertEquals(90, Calculator.multiply(10, 9));
    }

    @Test
    public void testMultiplyWithNegative() {
        assertEquals(50, Calculator.multiply(-10, -5));
    }

    @Test
    public void testMultiplyWithPositiveAndNegative() {
        assertEquals(-100, Calculator.multiply(10, -10));
    }

    @Test
    public void testDivide() {
        assertEquals(10.0000, Calculator.divide(70, 7), 0.0001);
    }

    @Test
    public void testDivideWithNegative() {
        assertEquals(12.5000, Calculator.divide(-50, -4), 0.0001);
    }

    @Test
    public void testDivideWithPositiveAndNegative() {
        assertEquals(-10.0000, Calculator.divide(100, -10), 0.0001);
    }

    @Test
    public void testSqrt() {
        assertEquals(16, Calculator.sqrt(256), 0.0001);
    }

    @Test
    public void testSqrtWithDouble() {
        assertEquals(11.1803, Calculator.sqrt(125), 0.0001);
    }

    @Test(timeout = 5000)
    public void testIsPrime() {
        assertTrue(Calculator.isPrime(113));
        assertFalse(Calculator.isPrime(1));
        assertFalse(Calculator.isPrime(10));
    }

    @Test
    public void testFibonacciNum() {
        assertEquals(0, Calculator.getFibonacciNumber(0));
        assertEquals(1, Calculator.getFibonacciNumber(1));
        assertEquals(610, Calculator.getFibonacciNumber(15));
    }

    @Test
    public void testSummarizeNumberRowFinal() {
        NumberRowSupplier supplier = PowerMockito.spy(new NumberRowSupplier());
        PowerMockito.when(supplier.getRowFinal()).thenReturn(new int[]{10, 20, 30});
        int result = Calculator.summarizeNumberRow(supplier.getRowFinal());

        assertEquals(60, result);
    }

    @Test
    public void testSummarizeNumberRowStatic() {
        assertEquals(15, Calculator.summarizeNumberRow(NumberRowSupplier.getRowStatic()));
    }

    @Test
    public void testSummarizeNumberRowStaticWithPowMoc() {
        PowerMockito.mockStatic(NumberRowSupplier.class);
        PowerMockito.when(NumberRowSupplier.getRowStatic()).thenReturn(new int[]{20, 30, 40});
        int result = Calculator.summarizeNumberRow(NumberRowSupplier.getRowStatic());

        assertEquals(90, result);
    }

    @Test
    public void testSummarizeNumberRowPrivate() throws Exception {
        NumberRowSupplier supplier = PowerMockito.spy(new NumberRowSupplier());
        PowerMockito.when(supplier, "getRowPrivate").thenReturn(new int[]{40, 50, 60});
        int result = Calculator.summarizeNumberRow(supplier.getRowPrivateToPublic());

        assertEquals(150, result);
    }

    @Test
    public void testSummarizeNumberRowPublic() {
        NumberRowSupplier supplier = Mockito.mock(NumberRowSupplier.class);
        Mockito.when(supplier.getRowPublic()).thenReturn(new int[]{10, 20, 30});

        assertEquals(60, Calculator.summarizeNumberRow(supplier.getRowPublic()));
    }
}
