package com.epam.calculator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Calculator {
    private static Logger logger = LogManager.getLogger();

    public static int sum(int x, int y) {
        logger.info("Вызван метод sum() со следующими аргументами: " + x + ", " + y);
        int result = x + y;
        logger.info("Результат вызова метода sum(): " + result);

        return result;
    }

    public static int subtract(int x, int y) {
        logger.info("Вызван метод subtract() со следующими аргументами: " + x + ", " + y);
        int result = x - y;
        logger.info("Результат вызова метода subtract(): " + result);

        return result;
    }

    public static long multiply(int x, int y) {
        logger.info("Вызван метод multiply() со следующими аргументами: " + x + ", " + y);
        int result = x * y;
        logger.info("Результат вызова метода multiply(): " + result);

        return result;
    }

    public static double divide(int x, int y) {
        logger.info("Вызван метод divide() со следующими аргументами: " + x + ", " + y);

        if (y == 0) {
            logger.error("Делитель равен нулю. Некорректный ввод");
            throw new IllegalArgumentException("Делитель равен нулю");
        }

        double result = (double) x / y;
        logger.info("Результат вызова метода divide(): " + result);

        return result;
    }

    public static double sqrt(int x) {
        logger.info("Вызван метод sqrt() со следующим аргументом: " + x);

        if (x < 0) {
            logger.error("Аргумент меньше нуля. Некорректный ввод");
            throw new IllegalArgumentException("Аргумент меньше нуля");
        }

        double result = Math.sqrt(x);
        logger.info("Результат вызова метода sqrt(): " + result);

        return result;
    }

    public static boolean isPrime(int x) {
        logger.info("Вызван метод isPrime() со следующим аргументом: " + x);

        if (x < 1) {
            logger.error("Аргумент не является натуральным числом (меньше единицы). Некорректный ввод");
            throw new IllegalArgumentException("Аргумент меньше единицы");
        }

        if (x == 1) {
            logger.error("Аргумент является натуральным числом, но не относится к простым числам");
            return false;
        }

        if (x % 2 == 0) {
            logger.info("Аргумент не является простым числом");
            return false;
        }

        for (int i = 3; i * i <= x; i += 2) {
            logger.debug("Выполняется метод isPrime(). Аргумент " + x + ", параметр цикла i = " + i);

            if (x % i == 0) {
                logger.info("Аргумент не является простым числом");
                return false;
            }
        }

        logger.info("Аргумент является простым числом");
        return true;
    }

    public static long getFibonacciNumber(int x) {
        logger.info("Вызван метод getFibonacciNumber() со следующим аргументом: " + x);

        if (x < 0) {
            logger.error("Аргумент меньше нуля. Некорректный ввод");
            throw new IllegalArgumentException("Аргумент меньше нуля");
        }

        if (x == 0) {
            logger.info("Результат вызова метода getFibonacciNumber(): 0");
            return 0;
        }

        if (x == 1) {
            logger.info("Результат вызова метода getFibonacciNumber(): 1");
            return 1;
        }

        return getFibonacciNumber(x - 1) + getFibonacciNumber(x - 2);
    }

    public static int summarizeNumberRow(int[] row) {
        logger.info("Вызван метод summarizeNumberRow() со следующим аргументом: " + Arrays.toString(row));

        int sum = 0;

        for (int i = 0; i < row.length; i++) {
            logger.debug("Выполняется метод summarizeNumberRow(). Параметр цикла i = " + i);
            sum += row[i];
        }

        logger.info("Результат вызова метода summarizeNumberRow(): " + sum);
        return sum;
    }
}
