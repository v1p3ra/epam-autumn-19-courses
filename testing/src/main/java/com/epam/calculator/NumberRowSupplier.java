package com.epam.calculator;

public class NumberRowSupplier {

    final int[] getRowFinal() {
        return new int[]{1, 2, 3};
    }

    static int[] getRowStatic() {
        return new int[]{4, 5, 6};
    }

    private int[] getRowPrivate() {
        return new int[]{7, 8, 9};
    }

    public int[] getRowPrivateToPublic() {
        return getRowPrivate();
    }

    public int[] getRowPublic() {
        return new int[]{10, 11, 12};
    }
}
