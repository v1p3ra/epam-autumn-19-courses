Разработан калькулятор, выполняющий следующие операции:
- сложение;
- вычитание;
- умножение;
- деление;
- определение, является ли число простым (метод isPrime());
- вычисление числа Фибоначчи заданного порядка.

Созданы два сценария тестирования: с положительным и отрицательным результатами. Для тестирования использовались следующие инструменты: JUnit, Mockito, PowerMock.