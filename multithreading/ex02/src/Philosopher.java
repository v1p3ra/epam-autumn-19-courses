import java.util.List;

public class Philosopher implements Runnable {
    private int id;
    private List<Fork> forks;
    private Fork forkWithSmallerNumber;
    private Fork forkWithLargerNumber;

    public Philosopher(int id, List<Fork> forks) {
        this.id = id;
        this.forks = forks;
    }

    private void chooseForks() {
        Fork leftFork = forks.get(id - 1);
        Fork rightFork;

        if (id == 1) {
            rightFork = forks.get(forks.size() - 1);
        } else {
            rightFork = forks.get(id - 2);
        }

        if (leftFork.getId() > rightFork.getId()) {
            forkWithSmallerNumber = rightFork;
            forkWithLargerNumber = leftFork;
        } else {
            forkWithSmallerNumber = leftFork;
            forkWithLargerNumber = rightFork;
        }
    }

    @Override
    public void run() {
        chooseForks();

        while (true) {
            System.out.printf("Философ №%d размышляет...\n", id);

            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            synchronized (forkWithSmallerNumber) {
                System.out.printf("Философ №%d берёт вилку %d\n", id, forkWithSmallerNumber.getId());

                synchronized (forkWithLargerNumber) {
                    System.out.printf("Философ №%d берёт вилку %d\n", id, forkWithLargerNumber.getId());
                    System.out.printf("Философ №%d ест\n", id);
                    try {
                        Thread.sleep(300);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.printf("Философ №%d кладёт вилку %d\n", id, forkWithLargerNumber.getId());
                }

                System.out.printf("Философ №%d кладёт вилку %d\n", id, forkWithSmallerNumber.getId());
            }
        }
    }
}
