import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        int philosophersCount = 5;
        List<Fork> forks = new ArrayList<>(philosophersCount);

        for (int i = 1; i <= philosophersCount; i++) {
            forks.add(new Fork(i));
        }

        for (int i = 1; i <= philosophersCount; i++) {
            Philosopher philosopher = new Philosopher(i, forks);
            Thread thread = new Thread(philosopher);
            thread.start();
        }
    }
}
