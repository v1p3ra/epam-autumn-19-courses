package fixed;

public class Main {

    public static void main(String[] args) {
        int moneyAmount = 5000;
        int customersCount = 5;
        Bank bank = new Bank(moneyAmount);

        for (int i = 1; i <= customersCount; i++) {
            BankUser bankUser = new BankUser(bank);
            Thread thread = new Thread(bankUser, "Клиент " + i);
            thread.start();
        }
    }
}
