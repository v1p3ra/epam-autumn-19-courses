package fixed;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bank {
    private Lock lock;
    private volatile int moneyAmount;

    public Bank(int moneyAmount) {
        lock = new ReentrantLock();
        this.moneyAmount = moneyAmount;
    }

    public Lock getLock() {
        return lock;
    }

    public int getMoneyAmount() {
        return moneyAmount;
    }

    public void transferMoney(int amount) {
        int newBalance = moneyAmount - amount;

        if (newBalance >= 0) {
            moneyAmount = newBalance;
            System.out.printf("Новый баланс счёта: %d руб.\n", moneyAmount);
        } else {
            throw new NotEnoughMoneyException("Недостаточно средств на банковском счёте");
        }
    }

    public boolean hasMoney(int amount) {
        return (moneyAmount >= amount);
    }
}
