package original;

public class BankUser implements Runnable {
    private Bank bank;
    private int withdrawAmount = 500;
    private String name;

    public BankUser(Bank bank) {
        this.bank = bank;
    }

    @Override
    public void run() {
        name = Thread.currentThread().getName();

        while (bank.hasMoney(withdrawAmount)) {
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.printf("%s пытается снять %d руб. со счёта\n", name, withdrawAmount);
            bank.transferMoney(withdrawAmount);
            System.out.printf("%s успешно снял средства со счёта\n\n", name);
        }

        System.out.printf("%s закончил снятие денег. На банковском счету: %d руб.\n", name, bank.getMoneyAmount());
    }
}
