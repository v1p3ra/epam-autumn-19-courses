package original;

public class Bank {
    private int moneyAmount;

    public Bank(int moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public int getMoneyAmount() {
        return moneyAmount;
    }

    public void transferMoney(int amount) {
        int newBalance = moneyAmount - amount;

        if (newBalance >= 0) {
            moneyAmount = newBalance;
            System.out.printf("Новый баланс счёта: %d руб.\n", moneyAmount);
        } else {
            throw new NotEnoughMoneyException("Недостаточно средств на банковском счёте");
        }
    }

    public boolean hasMoney(int amount) {
        return (moneyAmount >= amount);
    }
}
