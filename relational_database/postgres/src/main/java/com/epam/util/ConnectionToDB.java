package com.epam.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionToDB {
    private static final String URL = "jdbc:postgresql://localhost/";
    private static final String URL_DB = "jdbc:postgresql://localhost/music";
    private static final String USERNAME = "postgres";
    private static final String PASSWORD = "";

    public static Connection createConnection() throws SQLException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("JDBC driver not found");
            e.printStackTrace();
        }
        return DriverManager.getConnection(URL, USERNAME, PASSWORD);
    }

    public static Connection createConnectionToDB() throws SQLException {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("JDBC driver not found");
            e.printStackTrace();
        }
        return DriverManager.getConnection(URL_DB, USERNAME, PASSWORD);
    }
}
