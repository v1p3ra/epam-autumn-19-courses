package com.epam;

import com.epam.util.ConnectionToDB;
import org.junit.*;

import java.sql.*;

public class JDBCTest {
    private static DBAssistant dbAssistant = new DBAssistant();
    private static TableAssistant tableAssistant = new TableAssistant();

    @BeforeClass
    public static void initDB() throws Exception {
        dbAssistant.createDB();
        tableAssistant.addStoredProcedure();
    }

    @AfterClass
    public static void removeDB() throws Exception {
        dbAssistant.dropDB();
    }

    @Before
    public void initTables() throws Exception {
        tableAssistant.createTables();
        tableAssistant.fillTables();
        tableAssistant.addTrigger();
    }

    @After
    public void removeTables() throws Exception {
        tableAssistant.dropTables();
    }

    @Test
    public void testSelect() {
        try (Connection connection = ConnectionToDB.createConnectionToDB();
             Statement statement = connection.createStatement()) {

            System.out.println("Выполнено подключение к БД");
            System.out.println("Получение данных из таблицы songs...");
            String query = "SELECT * FROM songs;";
            ResultSet resultSet = statement.executeQuery(query);
            int counter = 0;

            while (resultSet.next()) {
                counter++;
                System.out.println("id песни: " + resultSet.getLong("id"));
                System.out.println("id альбома: " + resultSet.getLong("album_id"));
                System.out.println("Название песни: " + resultSet.getString("song"));
                System.out.println("Продолжительность: " + resultSet.getString("duration") + "\n");
            }

            System.out.printf("Загружено %s записей\n\n", counter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testAggregateFunction() {
        try (Connection connection = ConnectionToDB.createConnectionToDB();
             Statement statement = connection.createStatement()) {

            System.out.println("Выполнено подключение к БД");
            System.out.println("Поиск самых новых альбомов...");
            String query = "SELECT * FROM albums WHERE year = (SELECT MAX(year) FROM albums);";
            ResultSet resultSet = statement.executeQuery(query);
            int counter = 0;

            System.out.println("Результаты обработки запроса:");
            while (resultSet.next()) {
                counter++;
                System.out.println("id альбома: " + resultSet.getLong("id"));
                System.out.println("id исполнителя: " + resultSet.getLong("artist_id"));
                System.out.println("Альбом: " + resultSet.getString("title"));
                System.out.println("Год: " + resultSet.getInt("year") + "\n");
            }

            System.out.printf("Загружено %s записей\n\n", counter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testInnerJoin() {
        try (Connection connection = ConnectionToDB.createConnectionToDB();
             Statement statement = connection.createStatement()) {

            System.out.println("Выполнено подключение к БД");
            System.out.println("Получение полной информации о песнях...");
            String query = "SELECT artists.id AS artist_id, name, album_id, title, year, songs.id AS song_id, song, duration "
                    + "FROM artists "
                    + "INNER JOIN albums ON artists.id = albums.artist_id "
                    + "INNER JOIN songs ON albums.id = songs.album_id "
                    + "ORDER BY artist_id, album_id, song_id;";

            ResultSet resultSet = statement.executeQuery(query);
            int counter = 0;

            while (resultSet.next()) {
                counter++;
                System.out.println("id исполнителя: " + resultSet.getLong("artist_id"));
                System.out.println("Исполнитель: " + resultSet.getString("name"));
                System.out.println("id альбома: " + resultSet.getLong("album_id"));
                System.out.println("Альбом: " + resultSet.getString("title"));
                System.out.println("Год: " + resultSet.getInt("year"));
                System.out.println("id песни: " + resultSet.getLong("song_id"));
                System.out.println("Название песни: " + resultSet.getString("song"));
                System.out.println("Продолжительность: " + resultSet.getString("duration") + "\n");
            }

            System.out.printf("Загружено %s записей\n\n", counter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    // Поиск альбомов исполнителей, для которых не добавлены песни в БД
    public void testLeftOuterJoin() {
        try (Connection connection = ConnectionToDB.createConnectionToDB();
             Statement statement = connection.createStatement()) {

            System.out.println("Выполнено подключение к БД");
            System.out.println("Получение информации о песнях...");
            System.out.println("Неполные записи в базе данных для следующих альбомов:");
            String query = "SELECT artists.id AS artist_id, name, albums.id as album_id, title, year, songs.id AS song_id, song, duration "
                    + "FROM artists "
                    + "INNER JOIN albums ON artists.id = albums.artist_id "
                    + "LEFT JOIN songs ON albums.id = songs.album_id "
                    + "WHERE song IS NULL "
                    + "ORDER BY artist_id, album_id, song_id;";

            ResultSet resultSet = statement.executeQuery(query);
            int counter = 0;

            while (resultSet.next()) {
                counter++;
                System.out.println("id исполнителя: " + resultSet.getLong("artist_id"));
                System.out.println("Исполнитель: " + resultSet.getString("name"));
                System.out.println("id альбома: " + resultSet.getLong("album_id"));
                System.out.println("Альбом: " + resultSet.getString("title"));
                System.out.println("Год: " + resultSet.getInt("year") + "\n");
            }

            System.out.printf("Загружено %s записей\n\n", counter);

            System.out.println("Песни в базе данных без идентификатора альбома:");
            query = "SELECT * FROM songs WHERE album_id IS NULL;";
            resultSet = statement.executeQuery(query);
            counter = 0;

            while (resultSet.next()) {
                counter++;
                System.out.println("id песни: " + resultSet.getLong("id"));
                System.out.println("Название песни: " + resultSet.getString("song"));
                System.out.println("Продолжительность: " + resultSet.getString("duration") + "\n");
            }

            System.out.printf("Загружено %s записей\n\n", counter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPreparedStatementAndUpdate() {
        String query = "UPDATE albums SET year = ? WHERE year > ? AND year < ?;";

        try (Connection connection = ConnectionToDB.createConnectionToDB();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {

            System.out.println("Выполнено подключение к БД");
            preparedStatement.setString(1, "1970");
            preparedStatement.setString(2, "2001");
            preparedStatement.setString(3, "2005");

            System.out.println("Обновление записей...");
            int result = preparedStatement.executeUpdate();
            System.out.println("Изменено записей: " + result + "\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCallableStatement() {
        String query = "{CALL GetLongSongs()}";

        try (Connection connection = ConnectionToDB.createConnectionToDB();
             CallableStatement callableStatement = connection.prepareCall(query)) {

            System.out.println("Выполнено подключение к БД");
            System.out.println("Запушена хранимая процедура GetLongSongs()...");
            ResultSet resultSet = callableStatement.executeQuery();
            int counter = 0;

            System.out.println("Результаты выполнения процедуры:");
            while (resultSet.next()) {
                counter++;
                System.out.println("id песни: " + resultSet.getLong("id"));
                System.out.println("id альбома: " + resultSet.getLong("album_id"));
                System.out.println("Название песни: " + resultSet.getString("song"));
                System.out.println("Продолжительность: " + resultSet.getString("duration") + "\n");
            }

            System.out.printf("Загружено %s записей\n\n", counter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testTrigger() {
        try (Connection connection = ConnectionToDB.createConnectionToDB();
             Statement statement = connection.createStatement()) {

            System.out.println("Выполнено подключение к БД");
            System.out.println("Проверка работы триггера запрета удаления записей из таблицы albums...");

            String query = "DELETE FROM albums WHERE year < 2007;";
            System.out.println("Попытка удаления альбомов, выпущенных до 2007 года...");
            statement.executeUpdate(query);
        } catch (SQLException e) {
            System.out.println("Ошибка: " + e.getMessage() + "\n");
        }
    }

    @Test
    public void testTransaction() {
        try (Connection connection = ConnectionToDB.createConnectionToDB();
             Statement statement = connection.createStatement()) {

            System.out.println("Выполнено подключение к БД");
            System.out.println("Добавление новой записи в таблицу artists в рамках транзакции...\n");

            String query = "BEGIN;";
            statement.executeUpdate(query);

            query = "INSERT INTO artists (name) VALUES ('Kanye West');";
            statement.executeUpdate(query);

            query = "COMMIT;";
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
