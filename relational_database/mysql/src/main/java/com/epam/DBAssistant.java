package com.epam;

import com.epam.util.ConnectionToDB;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBAssistant {

    public void createDB() {
        try (Connection connection = ConnectionToDB.createConnection();
             Statement statement = connection.createStatement()) {

            System.out.println("Выполнено подключение к серверу БД");
            String query = "CREATE DATABASE music;";
            statement.executeUpdate(query);
            System.out.println("Создана база данных music\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void dropDB() {
        try (Connection connection = ConnectionToDB.createConnection();
             Statement statement = connection.createStatement()) {

            System.out.println("Выполнено подключение к серверу БД");
            String query = "DROP DATABASE music;";
            statement.executeUpdate(query);
            System.out.println("Удалена база данных music\n");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
